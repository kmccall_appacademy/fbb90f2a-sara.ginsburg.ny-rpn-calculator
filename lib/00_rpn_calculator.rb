class RPNCalculator
  OPERATORS = %w(+ - * / **)

  def initialize
    @stack = []
    # @elements = expression.split
  end

  attr_accessor :calculator


  def push(num)
    @stack << num
  end

  def value
    @stack.last
  end

  def plus
    perform_op(:+)
  end

  def minus
    perform_op(:-)
  end

  def divide
    perform_op(:/)
  end

  def times
    perform_op(:*)
  end



  def tokens(str)
    tokens = str.split.map.each do |ch|
      OPERATORS.include?(ch) ? ch.to_sym : ch.to_i
    end
  end

  def evaluate(str)
    tokens = tokens(str)
    operations_symbols = OPERATORS.map {|ch| ch.to_sym}
    tokens.each do |char|
      if operations_symbols.include?(char)
        perform_op(char)
      else
        @stack << char
      end
    end
    self.value
  end

  private
  def perform_op(op)
    raise "calculator is empty" if @stack.size < 2
    second_operand = @stack.pop
    first_operand = @stack.pop
    case op
    when :*
      @stack << first_operand * second_operand
    when :+
      @stack << first_operand + second_operand
    when :/
      @stack << first_operand.to_f / second_operand.to_f
    when :-
      @stack << first_operand - second_operand
    else

      @stack << first_operand
      @stack << second_operand
      raise "no such operand"
    end
  end

end


 @calculator = RPNCalculator.new
